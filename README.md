# UW Chatbot Help Button Popup
This module is the Chatbot Help Button Popup built for The Centre.
It creates a block in the global footer region that provides a floating popup in the lower right
of the viewport.  It will scroll along with the user's screen in a fixed position.
It can be put in a minimized state that will also be saved via a cookie.
The contents, button link, and page visibility can be customized via the dashboard configuration link.

---
# Functions

### function uw_chatbot_help_button_block_info()
Implements hook_block_info()
Used to enable the block and assign it to the global_footer region programmatically.

#### Returns
Blocks object

---
### function uw_chatbot_help_button_help($path, $arg)
Implements hook_help()
Displays module description in module
 
#### Returns
String containing help text

---
### function uw_chatbot_help_button_menu()
Implements hook_menu()
Registers an admin dashboard menu item for configuring the module as well as a URL for the configuration page

#### Returns
An associative array whose keys define paths and whose values are an associative array of properties for each path.

#### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7.x) for this specific hook for more information.

---
### function uw_chatbot_help_button_permissions($roles = array())
This gives custom access based on what the role permissions are.

#### Returns
If the user is an admin, it will return true and give them permission to view and edit the page. It will return false otherwise.

---
### function uw_chatbot_help_button_block_view($delta)
Populates the content of the module block to be displayed on pages where the block is included

#### Returns
The modified block object

---
### function uw_chatbot_help_button_theme_registry_alter(&$theme_registry)
Implements hook_theme_registry_alter()
This injects our template file into the theme so that it may be used in the block view.

#### Returns
No return objects, this modifies theme_registry by reference.

---
### function uw_chatbot_help_button_admin_form()
Implements hook_form
Defines the configuration form for the module

---
### function uw_chatbot_help_button_admin_form_validate()
Implements hook_form_validate()
Validates the admin config form.

---
### function uw_chatbot_help_button_form_submit()
Implements hook_form_submit()
Handles form submission and updates the block visibility configuration via db_update()
