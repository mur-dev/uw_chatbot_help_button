document.addEventListener("DOMContentLoaded", function(event) {
  // Helper function: get viewport top of element
  function offset(el) {
    let rect = el.getBoundingClientRect();
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return rect.top + scrollTop;
  }

  // Adjust bottom postion of element. Keeps the element above the footer, but otherwise will freeze to the bottom of viewport
  function checkOffset(el) {
    let popup = el;
    let footer = document.querySelector('#footer');

    // We hit the footer, keep popup above footer as scrolled
    if ( offset(popup) + popup.offsetHeight >= window.innerHeight - footer.getBoundingClientRect().top ) {
      popup.style.bottom = (window.innerHeight - footer.getBoundingClientRect().top + 20) + "px"
    }

    // we're above the footer, hover at 20px from bottom
    if ( document.documentElement.scrollTop + window.innerHeight < offset(footer) &&
      document.body.scrollTop + window.innerHeight < offset(footer)) {
      popup.style.bottom = "20px";
    }
  }

  // Sets or unsets hiding cookie with 24 expiry time
  function setCookie(value) {
    let expDate = new Date();

    expDate.setTime(expDate.getTime() + (24 * 60 * 60 * 1000));
    document.cookie = "uw_chatbot_help_button_hide=" + value + "; expires=" + expDate.toGMTString();
  }

  // Returns our specific uw_chatbot_help_button_hide cookie's value
  function getCookie() {
    return document.cookie.replace(/(?:(?:^|.*;\s*)uw_chatbot_help_button_hide\s*\=\s*([^;]*).*$)|^.*$/, "$1")
  }

  // Check if we should be displaying the popup or not based on screen width and cookie settings
  function checkDisplay(elements = []) {
    if (window.innerWidth >= 768 ) {
      // Check cookie to see if already hidden
      if (getCookie() !== "1") {
        popup.style.display = "block";
        popupMinimized.style.display = "none";
      } else {
        popup.style.display = "none";
        popupMinimized.style.display = "block";
      }

      elements.forEach(checkOffset);
    } else {
      elements.forEach((e) => { e.style.display = "none"});
    }
  }

  // Get our two popup elements
  let popup = document.querySelector('#uw_chatbot_help_button');
  let popupMinimized = document.querySelector('#uw_chatbot_help_button_minimized');

  // Create initial event handlers to reposition as the page is scrolled
  window.addEventListener("resize", () => { checkDisplay([popup, popupMinimized]); });
  document.addEventListener('scroll', () => { checkDisplay([popup, popupMinimized]); });
  checkDisplay([popup, popupMinimized]);

  // Create event handler for close button
  let closeButton = document.getElementById('uw_chatbot_help_button_close');
  closeButton.addEventListener('click', (e) => {
    popup.style.display = "none";
    popupMinimized.style.display = "block";
    checkOffset(popupMinimized);
    setCookie(1);
  });

  // Event listener for minimized popup being clicked, will show the fullsize popup and remove cookie
  popupMinimized.addEventListener('click', (e) => {
    popupMinimized.style.display = "none";
    popup.style.display = "block";
    checkOffset(popup);
    setCookie(0);
  });
});
